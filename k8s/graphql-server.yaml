---
kind: Deployment
apiVersion: extensions/v1beta1
metadata:
  name: website-graphql-server
  namespace: website
spec:
  replicas: 1
  selector:
    matchLabels:
      app: website
      part: website-graphql-server
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  template:
    metadata:
      labels:
        app: website
        part: website-graphql-server
    spec:
      imagePullSecrets:
        - name: gitlabcomsecret
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: website-data
        - name: chromium-user-data-dir
          emptyDir: {}
      containers:
        - name: main
          image: registry.gitlab.com/kachkaev/website-graphql-server:master
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: data
              mountPath: /var/data
            - name: chromium-user-data-dir
              mountPath: /var/chromium-user-data-dir
          env:
            - name: PATH_TO_PROFILE_INFOS
              value: /var/data/profile-infos
            - name: CHROMIUM_USER_DATA_DIR
              value: /var/chromium-user-data-dir
            - name: ENDPOINT_PATH
              value: /graphql
            - name: LINKEDIN_LOGIN
              valueFrom:
                secretKeyRef:
                  name: linkedin
                  key: login
            - name: LINKEDIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: linkedin
                  key: password
            - name: LINKEDIN_PROXY_SERVER
              valueFrom:
                secretKeyRef:
                  name: linkedin
                  key: proxy_server
            - name: FLICKR_USER_ID
              valueFrom:
                secretKeyRef:
                  name: flickr-api
                  key: user_id
            - name: FLICKR_API_KEY
              valueFrom:
                secretKeyRef:
                  name: flickr-api
                  key: api_key
            - name: TWITTER_CONSUMER_KEY
              valueFrom:
                secretKeyRef:
                  name: twitter-api
                  key: consumer_key
            - name: TWITTER_CONSUMER_SECRET
              valueFrom:
                secretKeyRef:
                  name: twitter-api
                  key: consumer_secret
            - name: TWITTER_ACCESS_TOKEN_KEY
              valueFrom:
                secretKeyRef:
                  name: twitter-api
                  key: access_token_key
            - name: TWITTER_ACCESS_TOKEN_SECRET
              valueFrom:
                secretKeyRef:
                  name: twitter-api
                  key: access_token_secret
            - name: FETCH_SECURITY_TOKEN
              valueFrom:
                secretKeyRef:
                  name: fetch-security-token
                  key: value
            - name: ENGINE_API_KEY
              valueFrom:
                secretKeyRef:
                  name: apollo-engine
                  key: api_key
          readinessProbe:
            tcpSocket:
              port: 4000
            initialDelaySeconds: 30
            periodSeconds: 5
          livenessProbe:
            tcpSocket:
              port: 4000
            initialDelaySeconds: 15
            periodSeconds: 20
          resources:
            requests:
              cpu: 10m
              memory: 100Mi
---
apiVersion: v1
kind: Service
metadata:
  name: website-graphql-server
  namespace: website
spec:
  ports:
    - name: main
      targetPort: 4000
      port: 4000
  selector:
    app: website
    part: website-graphql-server
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: website-graphql-server
  namespace: website
  annotations:
    traefik.ingress.kubernetes.io/rule-type: Path
spec:
  rules:
    - host: kachkaev.ru
      http:
        paths:
          - path: /graphql
            backend:
              serviceName: website-graphql-server
              servicePort: main
    - host: en.kachkaev.ru
      http:
        paths:
          - path: /graphql
            backend:
              serviceName: website-graphql-server
              servicePort: main
