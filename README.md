# \[[en.](https://en.kachkaev.ru)][kachkaev.ru](https://kachkaev.ru) source code

This meta project describes the architecture behind [kachkaev.ru](https://kachkaev.ru) and its English version [en.kachkaev.ru](https://en.kachkaev.ru).
The material can be useful for those who are curious about adding some state-of-art technologies and workflows to their personal toolbox.
Similar architecture can be used in a variety of apps including interactive web visualizations.

The website works inside a [kubernetes](https://kubernetes.io/) cluster and consists of two [dockerized](https://www.docker.com/) microservices:

- https://gitlab.com/kachkaev/website-frontend
- https://gitlab.com/kachkaev/website-graphql-server

The source code for both microservices is open.
See repos’ `README.md` for the lists of their ingredients and other technical details.

## The big picture

![system diagram](assets/system-diagram.png?)

Both `frontend` and `graphql-server` microservices live in a cloud-native [kubernetes](https://kubernetes.io/) environment and are exposed to the outer world via a reverse proxy ([traefik](https://traefik.io/)).
When a request comes from a client, traefik terminates HTTPS encryption and passes the request further to one of the microservices depending on the url.

When `frontend` microservice receives a request, it sends back a static file or renders an HTML page, which in some cases involves making a sub-request to `graphql-server` to fetch some data.

If `path` in the client’s request matches `/graphql`, traefik forwards it to the `graphql-server` microservice directly.
This enables data fetching from the browser or from debugging tools such as [GraphQL Playground](https://github.com/graphcool/graphql-playground) or [GraphiQL.app](https://github.com/skevy/graphiql-app).

A few times a day `graphql-server` collects data from external social profiles and saves the result into a persistent volume.
It uses environment variables from kubernetes secrets to connect to some of the APIs.
The fact that the data is stored in a persistent volume rather than inside the microservice container itself makes the system more resilient to crashes and eases docker image updates.

Another background task that runs in the cluster is updating HTTPS certificates from [Let’s Encrypt](https://letsencrypt.org/).
This is done by Traefik, since terminating HTTPS is in the list of its responsibilities.

Docker images for `frontend` and `graphql-server` microservices are being downloaded from gitlab.com, where three git repositories including this one are hosted.
New commits trigger continuous integration (CI) pipelines and produce new docker images, which are ready to be deployed.
If the new code does not pass quality assurance checks, a pipeline fails and thus certain types of errors cannot reach production.

## Deploying to the cloud

![](https://gitlab.com/kachkaev/website/uploads/a416ccf87b7a1cd2e2bb386f8109f936/thinking.png)

Because the website is split into containerized microservices, it can be easily deployed to a wide range of environments from laptops to huge autoscalable and geographically-distributed clouds.

Given that [kubernetes](https://kubernetes.io/) is the most powerful system for running cloud-native workloads, the instructions below are written for this platform.
The same steps apply regardless of the physical details of the server environment, as long as it has kubernetes API.

The objects being deployed are described as yaml files, which are located in [`k8s`](k8s) directory of this repo.
They can be useful as examples for building apps with similar architecture, especially when combined with [kubernetes documentation](https://kubernetes.io/docs/home/).

The commands below assume that a kubernetes cluster is already setup, `kubectl` client is configured to connect to it and the current kubernetes user is able to create resources in the `website` namespace.
It is also assumed that the cluster’s [ingress controller](https://kubernetes.io/docs/concepts/services-networking/ingress/) is in place, so the creation of `Ingress` objects leads to exposing kubernetes services to the outer world via HTTP and HTTPS.
If you are not using [traefik](https://traefik.io/) as the ingress controller, you might need to replace a couple of annotations in the yamls (e.g. `traefik.website-frontend.rule.type`).
You may also want to modify some other bits such as those containing host names.

![doing](https://gitlab.com/kachkaev/website/uploads/c0799225fbfc40e2c493ed290bc345d1/doing.png)

First, ensure that a namespace called `website` exists in your cluster:

```bash
kubectl apply -f k8s/namespace.yaml
```

Now you can deploy the `frontend` microservice with just one command:

```bash
kubectl apply -f k8s/frontend.yaml
```

This ensures a deployment, a service and two ingress rules and thus brings up a static version of the website powered by [next.js](https://github.com/zeit/next.js/).
Because the GraphQL microservice is not available yet, the website will ‘gracefully degrade’ by showing blank space instead of profile details.

The deployment of the `graphql-server` microservice starts with creating a persistent volume claim.
It is needed to store fetched profile infos and the error log.

```bash
kubectl apply -f k8s/pvc.yaml
```

Because some of the environment variables expected by the `graphql-server` container are sensitive, it is better to create them as kubernetes secrets:

```bash
FLICKR_USER_ID=??
FLICKR_API_KEY=??
kubectl create secret generic flickr-api \
  --from-literal=user_id=${FLICKR_USER_ID} \
  --from-literal=api_key=${FLICKR_API_KEY} \
  --namespace=website

LINKEDIN_LOGIN=??
LINKEDIN_PASSWORD=??
LINKEDIN_PROXY_SERVER=??
kubectl create secret generic linkedin \
  --from-literal=login=${LINKEDIN_LOGIN} \
  --from-literal=password=${LINKEDIN_PASSWORD} \
  --from-literal=proxy_server=${LINKEDIN_PROXY_SERVER} \
  --namespace=website

TWITTER_CONSUMER_KEY=??
TWITTER_CONSUMER_SECRET=??
TWITTER_ACCESS_TOKEN_KEY=??
TWITTER_ACCESS_TOKEN_SECRET=??
kubectl create secret generic twitter-api \
  --from-literal=consumer_key=${TWITTER_CONSUMER_KEY} \
  --from-literal=consumer_secret=${TWITTER_CONSUMER_SECRET} \
  --from-literal=access_token_key=${TWITTER_ACCESS_TOKEN_KEY} \
  --from-literal=access_token_secret=${TWITTER_ACCESS_TOKEN_SECRET} \
  --namespace=website

FETCH_SECURITY_TOKEN=??
kubectl create secret generic fetch-security-token \
  --namespace=website \
  --from-literal=value=${FETCH_SECURITY_TOKEN}

ENGINE_API_KEY=?? ## optional to use apollo engine (https://engine.apollographql.com/)
## create a secret with a blank value if you don't want to use this tracking tool
kubectl create secret generic apollo-engine \
--namespace=website \
--from-literal=api_key=${ENGINE_API_KEY}
```

Now the GraphQL server is ready to go live:

```bash
kubectl apply -f k8s/graphql-server.yaml
```

All done!

![](https://gitlab.com/kachkaev/website/uploads/83aa65c795d488f754a34a4e61d57cfd/done.png)

After the initial launch, make sure you call
`fetchProfileInfos` mutation from [GraphQL Playground](https://kachkaev.ru/graphql) to populate profile infos before this is done on schedule for the first time.
Don’t forget to pass your `FETCH_SECURITY_TOKEN` as an input because otherwise the mutation will be rejected!

To manually update the website microservices, these commands can be used:

```bash
NEW_FRONTEND_IMAGE_TAG=?? ## e.g. latest commit hash
kubectl set image --namespace=website deployment/website-frontend main=registry.gitlab.com/kachkaev/website-frontend:${NEW_FRONTEND_IMAGE_TAG}

NEW_GRAPHQL_SERVER_IMAGE_TAG=??
kubectl set image --namespace=website deployment/website-graphql-server main=registry.gitlab.com/kachkaev/website-graphql-server:${NEW_GRAPHQL_SERVER_IMAGE_TAG}
```

Alternatively, it is possible to modify docker image urls directly in yaml files and then run `kubectl apply ...` again.
In any case, the updates will run with zero downtime because of their [rolling nature](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-update-deployment).

The real production cluster contains a couple of other microservices such as for redirecting www domains to non-www, but these are omitted from this document to keep it focused.

## Future work

- Take GitLab CI/CD one step further and show how to automatically redeploy microservices on each push to master
- Document horizontal scaling for workload and infra
- Extract headless chrome from `graphql-server` into a separate microservice to speed-up CI jobs and reduce docker image size
- Use [helm charts](https://github.com/kubernetes/charts) instead of plain yamls for more flexibility

## Getting involved

This is a pretty small personal project, meaning that there is nothing much to collaborate on.
However, I don’t exclude that you might want to learn something by playing with the repos or even make your own (much better) website based on my code.
If you have questions, feel free to ask me anything by creating a new [GitLab issue](https://gitlab.com/kachkaev/website/issues) or [sending an email](mailto:alexander@kachkaev.ru)!

All three GitLab repos that form my website are licensed under [MIT](LICENSE), so you are free to do whatever you want with this stuff!

![thumbs-up](https://gitlab.com/kachkaev/website/uploads/749d5f4679392be346e7e986f2e5e5e1/thumbs-up.png)
